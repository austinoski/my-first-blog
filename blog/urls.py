from django.conf.urls import url

from django.contrib.auth.decorators import login_required

from . import views


urlpatterns = [
    url(r'^$', views.HomePageView.as_view(), name="post_list"),
    url(r'^(?P<pk>[\d+])/$', views.DetailPageView.as_view(), name="post_detail"),
    url(r'^post/new/$', login_required(views.PostNewView.as_view()), name="post_new"),
    url(r'^(?P<pk>[\d+])/edit/$', login_required(views.PostEditView.as_view()), name="post_edit"),
    url(r'^(?P<pk>[\d+])/delete/$', login_required(views.PostDeleteView.as_view()), name="post_delete"),
    url(r'^drafts/$', login_required(views.DraftsPageView.as_view()), name="post_draft_list"),
    url(r'^(?P<pk>[\d+])/publish/$', login_required(views.PostPublishView.as_view()), name="post_publish")
]