from django.views import View

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView

from django.urls import reverse_lazy, reverse
from django.utils import timezone

from django.shortcuts import redirect
from django.shortcuts import get_object_or_404

from .models import Post


class HomePageView(ListView):
    model = Post
    template_name = "blog/home.html"
    context_object_name = "posts"

    def get_queryset(self):
        queryset = super(HomePageView, self).get_queryset()
        queryset = queryset.filter(date_published__isnull=False)
        queryset = queryset.filter(date_published__lte=timezone.now()).order_by('date_created')
        return queryset


class DraftsPageView(ListView):
    model = Post
    template_name = "blog/draft.html"
    context_object_name = "posts"

    def get_queryset(self):
        queryset = super(DraftsPageView, self).get_queryset()
        queryset = queryset.filter(date_published__isnull=True).order_by('date_created')
        return queryset


class DetailPageView(DetailView):
    model = Post
    context_object_name = "post"
    template_name = "blog/detail.html"


class PostNewView(CreateView):
    template_name = "blog/new.html"
    model = Post
    fields = ['title', 'text']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(PostNewView, self).form_valid(form)


class PostEditView(UpdateView):
    template_name = "blog/edit.html"
    model = Post
    fields = ['title', 'text']

    def form_valid(self, form):
        form.instance.save()
        return super(PostEditView, self).form_valid(form)


class PostDeleteView(DeleteView):
    template_name = "blog/confirm_delete.html"
    model = Post
    success_url = reverse_lazy('post_list')


class PostPublishView(View):
    def get(self, request, *args, **kwargs):
        post = get_object_or_404(Post, pk=kwargs['pk'])
        post.publish()
        return redirect(reverse('post_list'))